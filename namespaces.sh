#!/bin/bash

########## Description: 
##########      This script is supposed to create four namespaces, defined as the script parameters. 
##########	The script then creates veth pairs accordingly and adds them to the OVS, that will be also installed.
##########      It assigns an IP address to the namespace along with the VLAN ID.
##########      At the end it creates a vxlan tunnel between the two instances.
##########	The script must be run on the local host/VM and remote host/VM.
########## Usage:
########## 	./namespaces.sh <namespace_name_1> <namespace_name_2> <namespace_name_3> <namespace_name_4>
##########
########## Author:
##########	Mateusz Janowicz
########## Email:
##########	mateusz.janowicz@tieto.com

#Script parameters:

namespaces=$@
vxlan_interface=$(ip a | grep vxlan | awk '{print $2}' | sed 's/://g') 

#Function definitions:

SpinnerFunction() {

    spinner='-/|\'
    counter=0
    pid=$!

    while kill -0 "$pid" 2>/dev/null; do
        counter=$(((counter+1) %4))
        printf "\r%s" ${spinner:$counter:1}
        sleep .1
    done
}

Help() {

    printf "\nScript usage: ./namespaces.sh <namespace_name_1> <namespace_name_2> <namespace_name_3> <namespace_name_4>\nHit 'install' to perform the setup, 'destroy' to purge the current setup or 'exit' to quit.\n"
}

NSNameCheckAndCreate() {

    printf "Checking if the given namespaces exist...\n\n"
    sleep 1 &
    SpinnerFunction
    for namespace in ${namespaces[@]}; do
        ip netns list | grep $namespace | grep -v grep >/dev/null 2>&1
        if [[ $? -eq 0 ]]; then
            printf "Namespace called $namespace already exist, skipping.\n"
        else
            printf "The namespace $namespace is going to be created.\n"
	    ip netns add $namespace >/dev/null 2>&1
	    printf "Namespace $namespace has been created.\n\n"
        fi
    done
}

CheckOSAndInstallOVS() {

    printf "Checking your Linux distribution in order to install OVS...\n"
    if [[ -f /etc/redhat-release ]]; then
        printf "You have a RHEL based Linux.\n\n"
        OVSRHELInstall
    elif [[ -f /etc/debian_version ]]; then
        printf "You have a Debian based Linux.\n\n"
        OVSDebianInstall
    fi
}

CheckOSAndRemoveOVS() {

    printf "\nChecking your Linux distribution in order to remove OVS...\n"
    if [[ -f /etc/redhat-release ]]; then
        printf "You have a RHEL based Linux.\n\n"
        OVSRHELDeletion
    elif [[ -f /etc/debian_version ]]; then
        printf "You have a Debian based Linux.\n\n"
        OVSDebianDeletion
    fi
}

OVSRHELInstall() {

    printf "Checking if OVS is installed...\n"
    rpm -q openvswitch >/dev/null 2>&1
    if [[ $? -eq 0 ]]; then
        printf "OK, OVS is installed.\n"
    else
        printf "OVS is not installed and is going to be installed now...\n"
        yum install -y openvswitch.x86_64 >/dev/null 2>&1 &
        SpinnerFunction
        printf "\nOVS has been successfully installed.\n\n"
        OVSRHELCheckRun
    fi
}

OVSRHELDeletion() {

    printf "Checking if OVS is installed...\n"
    rpm -q openvswitch >/dev/null 2>&1
    if [[ $? -eq 0 ]]; then
        printf "OVS is installed and is going to be removed now....\n"
        yum remove -y openvswitch.x86_64 > /dev/null 2>&1 &
        SpinnerFunction
        printf "OVS has been successfully removed.\n"
    else
        printf "\nOVS is not installed, nothing to do.\n\n"
    fi
}

OVSDebianInstall() {
    
    printf "Checking if OVS is installed...\n"
    dpkg -l | grep openvswitch-switch >/dev/null 2>&1
    if [[ $? -eq 0 ]]; then
        printf "OK, OVS is installed.\n\n"
    else 
        printf "OVS is not installed and is going to be installed now...\n"
        apt-get install -y openvswitch-switch >/dev/null 2>&1 &
        SpinnerFunction
        printf "\nOVS has been successfully installed.\n\n"
        OVSDebianCheckRun
    fi
}

OVSDebianDeletion() {

    printf "Checking if OVS is installed...\n"
    dpkg -l | grep  openvswitch-switch >/dev/null 2>&1
    if [[ $? -eq 0 ]]; then
        printf "OVS is installed and is going to be removed now....\n\n"
        apt-get purge -y openvswitch-switch > /dev/null 2>&1 &
        SpinnerFunction
        printf "OVS has been successfully removed.\n"
    else
        printf "\nOVS is not installed, nothing to do.\n\n"
    fi
}

OVSRHELCheckRun() {

    printf "Checking if OVS is running....\n"
    systemctl status openvswitch >/dev/null 2>&1
    if [[ $? -eq 0 ]]; then
        printf "OK, OVS is running.\n\n"
    else
       printf "OVS is not running, trying to bring it up...\n"
       systemctl start openvswitch >/dev/null 2>&1 &
       SpinnerFunction
       printf "OK, OVS is running now.\n\n"
    fi
}

OVSDebianCheckRun() {

    printf "Checking if OVS is running....\n"
    systemctl status openvswitch-switch >/dev/null 2>&1
    if [[ $? -eq 0 ]]; then
        printf "OK, OVS is running.\n\n"
    else
       printf "OVS is not running, trying to bring it up...\n"
       systemctl start openvswitch-switch >/dev/null 2>&1 &
       SpinnerFunction
       printf "OK, OVS is running now.\n\n"
    fi
}

OVSBridgeSetup() {

    printf "Creating OVS Bridge called 'ovs_switch'\n"
    ip a | grep ovs_switch >/dev/null 2>&1
    if [[ $? -ne 0 ]]; then    
        ovs-vsctl add-br ovs_switch
        ip link set dev ovs_switch up
    else
        printf "Bridge called 'ovs_switch' already exist. Re-creating....\n\n"
        ovs-vsctl del-br ovs_switch
        ovs-vsctl add-br ovs_switch
        ip link set dev ovs_switch up
    fi
}

VethCreation() {

    for ((i=1; i<5; i++)); do
        ip a | grep DefaultNs$i >/dev/null 2>&1
        if [[ $? -ne 0 ]]; then
            printf "Creating veth pair'DefaultNs$i'.\n"
            ip link add DefaultNs$i type veth peer name Ns$i
        else
            printf "Veth pair called 'DefaultNs$i' already exist, skipping.\n"
        fi   
    done
}

VethSetup() {
    
    counter=1        
    for namespace in ${namespaces[@]}; do
        ip link set Ns$counter netns $namespace 
        ip netns exec $namespace ip link set dev Ns$counter
        ip link set dev DefaultNs$counter up
        ip netns exec $namespace ip link set Ns$counter up
        while true; do
            printf "\nThe IP Addresses for the interfaces in namespaces will have 10.10.10.x addresses.\n"
            read -p "Please now specify the octet of the $counter IP address for namespace - replace 'x' with a number from 1-254: " -e octet
            if [[ -z $octet ]]; then
                continue
            elif [[ $octet -gt 0 ]] && [[ $octet -lt 254 ]]; then
                ip netns exec $namespace ip addr add 10.10.10.$octet/24 dev Ns$counter
                break
            else
                printf "Invalid format for the IP octet!\n"
                continue
            fi
        done
    counter=$(( $counter + 1 ))
    done 
}

VLANAssignment() {

    printf "\nAssigning VLAN IDs to namespaces and adding them to OVS...\n\n"
    
    for ((i=1; i<4; i+=2)); do
        ovs-vsctl add-port ovs_switch DefaultNs$i tag=100
    done
 
    for ((i=2; i<5; i+=2)); do
        ovs-vsctl add-port ovs_switch DefaultNs$i tag=200
    done
    while true; do
        read -p "Enter the IP Address of remote host: " -e remoteip
        if [[ -z $remoteip ]]; then
            continue
        elif [[ $remoteip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
            PingHost
            if [[ $ping == 'success' ]]; then
                ovs-vsctl add-port ovs_switch vxlan0 -- set interface vxlan0 type=vxlan options:remote_ip=$remoteip >/dev/null 2>&1
                break
             else
                continue
             fi
        else
            printf "Invalid format for the IP address.\n\n"
            continue
        fi
    done
}

PingHost() {

    printf "Checking availability of the defined IP address...\n\n"
    ping -c 5 $remoteip >/dev/null 2>&1
    if [[ $? -eq 0 ]]; then
        printf "Remote host is reachable.\n"
        ping=success
    elif [[ $? -eq 1 ]]; then
        printf "Remote host is unreachable.\n\n"
        ping=fail
    fi
}     

PurgeEnv() {

    printf "\nThe current setup will be removed now...\n"
    sleep 2 &
    SpinnerFunction

    ovs-vsctl del-br ovs_switch && printf "\nBridge 'ovs_switch' removed.\n"
    
    for ((i=1; i<5; i++)); do
        ip link delete DefaultNs$i
        printf "Veth pair DefaultNs$i deleted.\n"
    done

    ip link delete $vxlan_interface >/dev/null 2>&1 && printf "Interface $vxlan_interface removed.\n"

    for namespace in ${namespaces[@]}; do
        ip netns delete $namespace
        printf "Namespace $namespace deleted.\n"
    done

    CheckOSAndRemoveOVS
 
    printf "\nSetup deleted.\n\n"
}

########## Main part:

if [[ $@ =~ 'help' ]]; then
    Help
    exit 1
fi

if [[ $# -ne 4 ]]; then
    printf "You have to specify exactly four arguments! Enter 'help' after the script name to display the usage pattern.\n"
    exit 1

else
    clear
    while true; do
        printf "$(date)\n
        Welcome $USER.\n
        This script is supposed to create four namespaces, defined as the script parameters. 
        The script then creates veth pairs accordingly and adds them to the OVS, that will be also installed.
        It assigns an IP address to the namespace along with the VLAN ID.
        At the end it creates a vxlan tunnel between the two instances.\n
        The script must be launched on the local host/VM and remote host/VM.\n\n"

        read -p "Hit 'install' to perform the setup, 'destroy' to purge the current setup or 'exit' to quit: " -t 10 -e initial

        if [[ $initial =~ 'exit' ]] || [[ $initial =~ 'EXIT' ]]; then
            printf "Exiting....\n"
            sleep 2 &
            SpinnerFunction
            exit 0

        elif [[ -z $initial ]]; then
            clear
            continue

        elif [[ $initial =~ 'help' ]] || [[ $initial =~ 'HELP' ]]; then
            Help
            sleep 5
            clear
            continue

        elif [[ $initial =~ 'destroy' ]] || [[ $initial =~ 'DESTROY' ]]; then
            PurgeEnv
            exit 0

        elif [[ $initial =~ 'install' ]] || [[ $initial =~ 'INSTALL' ]]; then
            printf "\nAttention! The environment is going to be installed.\n\n"
            sleep 5 &
            SpinnerFunction    
            NSNameCheckAndCreate ; CheckOSAndInstallOVS ; OVSBridgeSetup ; VethCreation ; VethSetup
            VLANAssignment ; printf "\nWe are done now...Please check the setup.\n\n" ; exit 0

        else
            printf "Unknown command, try again!"
            sleep 2
            clear
            continue
        fi
    done   
fi
exit 0
